package chess;

import game.Game;

/**
 * The Class Chess.
 */
public class Chess {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[]args) {
		Game game = new Game(true,true);
		String filePath = "/Users/Carlos/Desktop/Software Methodology/Chess58/src/game/moves.txt"; 
		game.gameStart(filePath);
	}
}