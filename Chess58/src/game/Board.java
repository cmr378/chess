package game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

public class Board {
	
	/**
	 * The main board that tracks player inputs
	 */
	
	public Piece [][] board = new Piece[8][8];
	public boolean drawBoard; 
	
    /**
     * Lists for captured pieces
     */
   public ArrayList <Piece> whiteCaptured = new ArrayList<Piece>();
   public ArrayList <Piece> blackCaptured = new ArrayList<Piece>(); 
   
   /**
    * List for enPassant capturable squares
    */
   public ArrayList<String> whiteEnPassant = new ArrayList<String>(); 
   public ArrayList<String> blackEnPassant = new ArrayList<String>(); 
   
   /**
    * All possible current b & w moves, refreshes every turn
    * These are used to check for checkMate
    */
   
   public Set<String> whiteKingMoves = new HashSet<String>(); 
   public Set<String> blackKingMoves = new HashSet<String>(); 

   int totalKingMoves = 0; 

    /**
     * Textboard for printing
     */
	 public String[][] textBoard = new String[9][9];

    /**
     * White pieces
     */

    String whitePawn = "wp ";
    String whiteRook = "wR ";
    String whiteKnight = "wN ";
    String whiteBishop = "wB ";
    String whiteQueen = "wQ ";
    String whiteKing = "wK ";
    String [] whiteRow = new String[8];

    /**
     * Black pieces
     */
    String blackPawn = "bp ";
    String blackRook = "bR ";
    String blackKnight = "bN ";
    String blackBishop = "bB ";
    String blackQueen = "bQ ";
    String blackKing = "bK ";
    String [] blackRow = new String[8];

    /**
     * Last row of textBoard stores them in hash-table for corresponding index
     * Not sure if i will need this or not, keeping it here just in case 
     */ 

    String [] alp = {" a "," b "," c "," d "," e " ," f "," g "," h "};

    Hashtable<String,Integer> hTable = new Hashtable<String,Integer>();
    Hashtable<Character,Integer> compVer = new Hashtable<Character,Integer>();


    /**
     * creates the Piece version of the board
     * Has to be done AFTER piece object logic done
     */
    public Board(){
    	createBoard();
    	drawTextBoard(); 
    }


    /**
     * Stores hashtable with corresponding file and it's index 
     * trims the string to make life easier later on
     */

    public void fillHtable(){
        for(int i = 0; i < 8; i++){
            hTable.put(alp[i].trim(),i);
        }
    }
    
    public void printUpdatedBoard() {
    	for(int i = 0; i < 9; i++) {
    		for(int j = 0; j < 9; j++) {
    			System.out.print(textBoard[i][j]); 
    		}
    		System.out.println();
    	}
    	System.out.println("\n"); 
    }
    
    /**
     * Prints the ascii art for the initial board
     */

    public void drawTextBoard() {

        getRow();
        int decrement = 8;

        for(int i = 0; i < 9; i++){
            for(int j = 0; j < 9; j++){

                if(i == 0){
                    if(j < 8){
                        textBoard[i][j] = blackRow[j];
                    }
                    else {
                        textBoard[i][j] = Integer.toString(decrement);
                        decrement--;
                    }
                }

                else if(i == 1){
                    if(j < 8){
                        textBoard[i][j] = blackPawn;
                    }
                    else{
                        textBoard[i][j] = Integer.toString(decrement);
                        decrement--;
                    }
                }

                else if(i == 2 || i == 4){
                    if(j < 8){
                        if(j % 2 != 0){
                            textBoard[i][j] = "## ";
                        }
                        else{
                            textBoard[i][j] = "   ";
                        }
                    }

                    else{
                        textBoard[i][j] = Integer.toString(decrement);
                        decrement--;
                    }
                }

                else if(i == 3 || i == 5){
                    if( j < 8 ){
                        if(j % 2 == 0){
                            textBoard[i][j] = "## ";
                        }
                        else {
                            textBoard[i][j] = "   ";
                        }
                    }
                    else {
                        textBoard[i][j] = Integer.toString(decrement);
                        decrement--;
                    }
                }

                else if( i == 6 ){
                    if(j < 8){
                        textBoard[i][j] = whitePawn;
                    }
                    else{
                        textBoard[i][j] = Integer.toString(decrement);
                        decrement--;
                    }
                }

                else if(i == 7){
                    if(j < 8){
                        textBoard[i][j] = whiteRow[j];
                    }
                    else{
                        textBoard[i][j] = Integer.toString(decrement);
                        decrement--;
                    }
                }

                else if(i == 8){
                    if(j < 8){
                        textBoard[i][j] = alp[j];
                    }
                    else{
                        textBoard[i][j] = "   ";
                    }
                }

                System.out.print(textBoard[i][j]);
            }
            System.out.print("\n");
        }
    }
    
    
    public void capture(Piece target) {
    	if(target.isWhite) {
    		System.out.println("Target color: " + target.isWhite);
    		whiteCaptured.add(target); 
    	}
    	else {
    		blackCaptured.add(target); 
    	}
    }
    
    
    /**
     * Converts string to board index
     * @param origin contains the index (i,j) of the piece to be moved
     * @param destination contains the index (i,j) of the piece's destination
     */
    
    public void convert(String origin, String destination, boolean turn){
    	fillHtable(); 
    	int[] originCor = new int[2]; 
    	int[] destCor = new int[2];
    	
    	int i = 0, originX = 0 , originY = 0 ; 
    	int destX = 0, destY = 0;     	
    	String comp; 
    	
    	while(i < 2){
    		if(i == 0) {
    			
    			//get x coordinate to identify object
    	    	comp = String.valueOf(origin.charAt(0));
    	    	originX = hTable.get(comp); 
    	    	originCor[i] = originX; 
    	    	
    	    	// get x coordinate for object destination
    	    	comp = String.valueOf(destination.charAt(0)); 
    	    	destX = hTable.get(comp);
    	    	destCor[i] = destX; 
    	    	i++; 
    	    	
    		}
    		
    		else if(i == 1) {
    			//get y coordinate to identify object
    			originY = Character.getNumericValue(origin.charAt(1));
    			originCor[i] = originY; 
    			
    			//get y 
    			destY = Character.getNumericValue(destination.charAt(1)); 
    			destCor[i] = destY; 
    			i++; 
       		}
    		
    		else {
        		i++; 
    		}
    	}
    	updateBoard(originCor,destCor,turn); 
    }
    
    
    /**
     * Temporary method to test the board. 
     */
    
    public void test() {
    	for(int i = 0; i < 8;i++) {
    		for(int j = 0; j < 8; j++) {
    			if(board[i][j] instanceof Piece){
    				System.out.print(board[i][j].getType());
    			}
    			else {
    				System.out.print("null"); 
    			}
    		}
    		System.out.println("\n"); 
    	}
    }

    /**
     * Board that will monitor the actual movement of the pieces
     */

    public void createBoard(){

    	for(int i = 0; i < 8; i++) {
    		for(int j = 0; j < 8; j++) {
    			
    			if(i == 0) {
    				if(j == 0 || j == 7) {
    					board[i][j] = new Rook(false,"Rook"); 
    				}
    				else if(j == 1 || j == 6) {
    					board[i][j] = new Knight(false,"Knight"); 
    				}
    				else if(j == 2 || j == 5) {
    					board[i][j] = new Bishop(false,"Bishop"); 
    				}
    				else if(j == 3) {
    					board[i][j] = new Queen(false,"Queen"); 
    				}
    				else if(j == 4) {
    					board[i][j] = new King(false,"King"); 
    				}
    			}
    			
    			else if(i == 1){
    				board[i][j] = new Pawn(false,"Pawn"); 
    			}
    			else if(i == 6) {
    				board[i][j] = new Pawn(true,"Pawn"); 
    			}
    			else if(i == 7) {
    				if(j == 0 || j == 7) {
    					board[i][j] = new Rook(true,"Rook"); 
    				}
    				else if(j == 1 || j == 6) {
    					board[i][j] = new Knight(true,"Knight"); 
    				}
    				else if(j == 2 || j == 5) {
    					board[i][j] = new Bishop(true,"Bishop"); 
    				}
    				else if(j == 3) {
    					board[i][j] = new Queen(true,"Queen"); 
    				}
    				else if(j == 4) {
    					board[i][j] = new King(true,"King"); 
    				}
    			}
    			else {
    				board[i][j] = null; // empty spaces
    			}
    		}
    	}
    }
    
    public void updateTextBoard(int originX, int originY, int destX, int destY) {
    	
    	String playerPiece = textBoard[originX][originY];
    	String currentMove = Integer.toString(destX) +","+Integer.toString(destY); 
    	
    	    	
    	if(originX == 0 && originY == 0) {
    		
    		textBoard[destX][destY] = playerPiece; 
    		textBoard[originX][originY] = "   "; 
    		
    	}

    	
    	else if(originX % 2 != 0) {
    		
    		if(originY == 0) {
    			
    			textBoard[destX][destY] = playerPiece;
    			textBoard[originX][originY] = "## ";
    		}
    		
    		else if(originY % 2 != 0) {
    			
    			
    			textBoard[destX][destY] = playerPiece;
    			textBoard[originX][originY] = "   ";
    		}
    		
    		else if(originY % 2 == 0) {
    			textBoard[destX][destY] = playerPiece;
    			textBoard[originX][originY] = "## ";
    		}
    	}
    	
    	else if(originX % 2 == 0) {
    		
    		if(originY == 0) {
    			textBoard[destX][destY] = playerPiece;
        		textBoard[originX][originY] = "   "; 
        		return; 
    		}
    		
    		else if(originY % 2 == 0) {
    			textBoard[destX][destY] = playerPiece;
    			textBoard[originX][originY] = "   ";
    		}
    		
    		else if(originY % 2 != 0) {
    			textBoard[destX][destY] = playerPiece;
    			textBoard[originX][originY] = "## ";
    		}
    			
    	}
    	
    	
    	
    	 if(playerPiece.equals("wp ") && destX == 0) {
    		 textBoard[destX][destY] = "wQ "; 
    	 }
    	 
    	 else if(playerPiece.equals("bp ") && destX == 7) {
    		 textBoard[destX][destY] = "bQ "; 
    	 }
    }
    
    public boolean verifyDiagonals(int originX, int originY, int destX, int destY) {
    	Piece playerPiece = board[originX][originY]; 
    	//upward right movement 
		if(destX < originX && destY > originY) {
			
			int i = originX, j = originY;
			while(i >= destX && j <= destY) {
				
				if(i > 0 && j < 7) {
					i--;
					j++;
				}
				    				
				if(board[i][j] instanceof Piece) {
					if(board[i][j].isWhite != playerPiece.isWhite && i != destX && j != destY) {
						return false; 
					}
					else if(board[i][j].isWhite != playerPiece.isWhite && i == destX && j == destY) {
						capture(board[i][j]); 
						board[destX][destY] = playerPiece;
						board[originX][originY] = null; 
						return true; 
					}
					else if(board[i][j].isWhite == playerPiece.isWhite) {
						return false; 
					}
				}
				
				else {
					if(i == destX && j == destY) {
						board[destX][destY] = playerPiece; 
						board[originX][originY] = null;
						return true;
					}
				}
				
			}
		}
		
		//upward left 
		else if(destX < originX && destY < originY) {
			
			int i = originX, j = originY; 
			
			while(i >= destX && j >= destY) {
				
				if(i > 0 && j > 0) {
					i--;
					j--; 
				}
				
				if(board[i][j] instanceof Piece) {
					
					if(board[i][j].isWhite != playerPiece.isWhite && i != destX && j != destY) {
						return false; 
					}
					else if(board[i][j].isWhite != playerPiece.isWhite && i == destX && j == destY) {
						capture(board[i][j]); 
						board[destX][destY] = playerPiece;
						board[originX][originY] = null; 
						return true; 
					}
					else if(board[i][j].isWhite == playerPiece.isWhite) {
						
						return false; 
					}
				}
				
				else {
					if(i == destX && j == destY) {
						board[i][j] = playerPiece; 
						board[originX][originY] = null;
						return true;
					}
				}
				
			}
		}
		
		//downward right
		else if(destX > originX && destY > originY) {
			int i = originX, j = originY; 
			
			while(i <= destX && j <= destY) {
				
				if(i < 7 && j < 7) {
					i++;
					j++; 
				}
				
				if(board[i][j] instanceof Piece) {
					
					if(board[i][j].isWhite != playerPiece.isWhite && i != destX && j != destY) {
						return false; 
					}
					else if(board[i][j].isWhite != playerPiece.isWhite && i == destX && j == destY) {
						capture(board[i][j]); 
						board[destX][destY] = playerPiece;
						board[originX][originY] = null; 
						return true; 
					}
					else if(board[i][j].isWhite == playerPiece.isWhite) {
						
						return false; 
					}
				}
				
				else {
					if(i == destX && j == destY) {
						board[i][j] = playerPiece; 
						board[originX][originY] = null;
						return true;
					}
				}
			}
		}
		//downward left 
		else if(destX > originX && destY < originY) {
			int i = originX, j = originY; 
			
			while(i <= destX && j >= destY) {
				
				if(i < 7 && j > 0) {
					i++;
					j--; 
				}
				
				if(board[i][j] instanceof Piece) {
					
					if(board[i][j].isWhite != playerPiece.isWhite && i != destX && j != destY) {
						return false; 
					}
					else if(board[i][j].isWhite != playerPiece.isWhite && i == destX && j == destY) {
						capture(board[i][j]); 
						board[destX][destY] = playerPiece;
						board[originX][originY] = null; 
						return true; 
					}
					else if(board[i][j].isWhite == playerPiece.isWhite) {
						
						return false; 
					}
				}
				
				else {
					if(i == destX && j == destY) {
						board[i][j] = playerPiece; 
						board[originX][originY] = null;
						return true;
					}
				}
			}
		}
		return true; 
    }
    
    public boolean verifyVerticals(int originX,int originY,int destX, int destY) {
    	Piece playerPiece = board[originX][originY]; 
    	
    	/*upward movement, checks for blocking on all those squares or for conquer */ 
		if(originX > destX) {
			
			int i = originX; 
			
			while(i >= destX) {
				
				if(i > 0) {
					i--; 
				}
				    					
				if(board[i][originY] instanceof Piece){
					
					if(board[i][originY].isWhite != playerPiece.isWhite && i != destX) {
						return false; 
					}
					else if(board[i][originY].isWhite != playerPiece.isWhite && i == destX) {
						capture(board[destX][originY]); 
    					board[destX][destY] = playerPiece; 
						board[originX][originY] = null;
						return true; 
					}
					else if(board[i][originY].isWhite == playerPiece.isWhite) {
						return false; 
					}
				}
				
				else {
					if(i == destX) {
						board[destX][originY] = playerPiece; 
						board[originX][originY] = null; 
						return true; 
					}
				}
			}
		}
		
		//downward movement 
		else {
			int i = originX; 
			while(i <= destX) {
				
				//incerement by one so same piece isnt counted
				
				if(i < 7) {
					i++; 
				}
				
				if(board[i][originY] instanceof Piece) {
					if(board[i][originY].isWhite != playerPiece.isWhite && i != destX) {
						return false; 
					}
					else if(board[i][originY].isWhite != playerPiece.isWhite && i == destX) {
						capture(board[i][originY]); 
						board[destX][destY] = playerPiece;
						board[originX][originY] = null; 
						return true; 
					}
					
					else if(board[i][originY].isWhite == playerPiece.isWhite) {
						return false;
					}
					
				}
				else {
					if(i == destX) {
						board[destX][originY] = playerPiece; 
						board[originX][originY] = null; 
						return true; 
					}
				}
			}
		}
		return true; 
    }

    public boolean verifyHorizontals(int originX,int originY,int destX, int destY) {
    	Piece playerPiece = board[originX][originY]; 
    	
		
		if(originY > destY) {
			
			int i = originY; 
			
			while(i >= destY) {
				
				if(i > 0) {
					i--; 
				}
				
				if(board[originX][i] instanceof Piece) {
					
					if(board[originX][i].isWhite != playerPiece.isWhite && i != destY) {
						return false; 
					}
					
					else if(board[originX][i].isWhite != playerPiece.isWhite && i == destY) {
						capture(board[originX][i]); 
						board[destX][destY] = playerPiece; 
						board[originX][originY] = null; 
						return true; 
					}
					
					else if(board[originX][i].isWhite == playerPiece.isWhite) {
						return false; 
					}
					
				}
				else {
					if(i == destY) {
						board[destX][destY] = playerPiece; 
						board[originX][originY] = null; 
						return true;
					}
				}
			}
		}
		
		else {
			int i = originY; 
			
			while( i <= destY) {
				
				if(i < 7) {
					i++; 
				}
				
				if(board[originX][i] instanceof Piece) {
					
					if(board[originX][i].isWhite != playerPiece.isWhite && i != destY) {
						return false;
					}
					
					else if(board[originX][i].isWhite != playerPiece.isWhite && i == destY) {
						capture(board[originX][i]); 
						board[destX][destY] = playerPiece; 
						board[originX][originY] = null; 
						return true; 
					}
					
					else if(board[originX][i].isWhite == playerPiece.isWhite) {
						return false; 
					}
				} 
				else {
					if(i == destY) {
						board[destX][destY] = playerPiece;
						board[originX][originY] = null; 
						return true;
					}
				}
			}
		}
		
		return true; 
    }
    
    
    /**
     * Update helper functions, these function concentrate on frequently used Piece moves -
     * as in multiple pieces share these common move types
     * The knight and pawn methods are unique as those pieces move differently than others. 
     * @param originX
     * @param originY
     * @param destX
     * @param destY
     * @return
     */
    
  
    public void verifyKingMove(Piece targetSquare, int destX, int destY) {
    	String goodMove = Integer.toString(destX) + "," + Integer.toString(destY); 
    	if(targetSquare.isWhite) {
    		whiteKingMoves.add(goodMove); 
    	}
    	else {
    		blackKingMoves.add(goodMove);
    	}
    }
    
    public boolean pawnMove(int originX, int originY, int destX, int destY) {
    	Piece targetSquare = board[destX][destY]; // the square to be moved to 
    	Piece playerPiece = board[originX][originY];
    	String currentMove; 
    	//piece is white 
    	// pawn is a unique piece to deal with 
    	
    	if(!playerPiece.getType().equals("Pawn")) {
    		return false; 
    	}
    	
    	else if(playerPiece.isWhite) {
			currentMove = Integer.toString(destX) + "," + Integer.toString(destY);
						
			//White piece is currently in an enPassant move add to list
			if(playerPiece.enPassant()) {
				whiteEnPassant.add(Integer.toString(destX + 1) + "," + Integer.toString(destY));
			}
			
			else if(!playerPiece.enPassant()) {
				whiteEnPassant.clear();
			}
			
			if(blackEnPassant.contains(currentMove)) {
				textBoard[destX][destY] = textBoard[originX][originY];
				
				if(destX + 1 % 2 == 0) {
					if(destY % 2 != 0) {
						textBoard[destX+1][destY] = "## ";
					}
					else {
						textBoard[destX+1][destY] = "   ";
					}
				}
				else {
					if(destY % 2 == 0) {
						textBoard[destX+1][destY] = "## ";
					}
					else {
						textBoard[destX+1][destY] = "   ";

					}
				}
				
				board[originX][originY] = null; 
				board[destX][destY] = playerPiece;
				blackCaptured.add(board[destX + 1][destY]); 
				board[destX + 1][destY] = null;
				return true; 
			}
			
	
			 if(playerPiece.promotion() == true) {
				 //piece is viable for promotion, promote and removed
				 board[destX][destY] = new Queen(playerPiece.isWhite,"Queen");
				 board[originX][originY] = null; 
				 return true; 
			 }
			
			 if(targetSquare == null && !playerPiece.captureMove()) {
				board[destX][destY] = playerPiece;
				board[originX][originY] = null;
				return true; 
			}
			
			 if(!targetSquare.isWhite && playerPiece.captureMove()) {
				board[destX][destY] = playerPiece; 
				board[originX][originY] = null;
				blackCaptured.add(targetSquare); 
				return true; 
			}
			
			 if(targetSquare != null && !playerPiece.captureMove()) {
				return false; 
			}
    		
    	}
    	
    	else {
			currentMove = Integer.toString(destX) + "," + Integer.toString(destY);
			//White piece is currently in an enPassant move add to list
			
			if(playerPiece.enPassant()) {
				blackEnPassant.add(Integer.toString(destX - 1) + "," + Integer.toString(destY));
			}
			
			else if(!playerPiece.enPassant()) {
				blackEnPassant.clear();
			}
			
			if(whiteEnPassant.contains(currentMove)) {
				textBoard[destX][destY] = textBoard[originX][originY];
				
				if(destX + 1 % 2 == 0) {
					if(destY % 2 != 0) {
						textBoard[destX+1][destY] = "## ";
					}
					else {
						textBoard[destX+1][destY] = "   ";
					}
				}
				else {
					if(destY % 2 == 0) {
						textBoard[destX+1][destY] = "## ";
					}
					else {
						textBoard[destX+1][destY] = "   ";

					}
				}
				board[originX][originY] = null; 
				board[destX][destY] = playerPiece;
				blackCaptured.add(board[destX - 1][destY]); 
				board[destX-1][destY] = null;
				return true; 
			}
			
			if(playerPiece.promotion() == true) {
			 //piece is viable for promotion, promote and removed
			 board[destX][destY] = new Queen(playerPiece.isWhite,"Queen");
			 board[originX][originY] = null; 
			 return true; 
		 	}
			
			if(targetSquare == null && !playerPiece.captureMove()) {
				board[destX][destY] = playerPiece;
				board[originX][originY] = null; 
				return true; 
			}
			
			 if(targetSquare.isWhite && playerPiece.captureMove()) {
				board[destX][destY] = playerPiece; 
				board[originX][originY] = null;
				whiteCaptured.add(targetSquare); 
				return true; 
			}
			
			 if(targetSquare != null && !playerPiece.captureMove()) {
				return false; 
			}
    		
    	}
    	
    	return true; 
    }
    
    public boolean rookMove(int originX, int originY, int destX, int destY) {
    	Piece playerPiece = board[originX][originY]; 
    	
    	if(!playerPiece.getType().equals("Rook")) {
    		return false; 
    	}
    	
    	else {
    		
    		if(playerPiece.horizontalMove() && verifyHorizontals(originX,originY,destX,destY)) {
    			return true; 
    		}
    		
    		else if(playerPiece.verticalMove() && verifyVerticals(originX,originY,destX,destY)) {
    			//setCurrentMoves(originX,originY,destX,destY); 
    			return true; 
    		}
    		
    		else {
    			return false; 
    		}
    		
    	}
    }
    
    public boolean bishopMove(int originX, int originY, int destX, int destY) {
    	Piece playerPiece = board[originX][originY]; 
    	
    	if(!(playerPiece.getType().equals("Bishop"))) {
    		return false;
    	}
    	
    	else {
    		if(verifyDiagonals(originX,originY,destX,destY)) {
    			return true; 
    		}
    		
    		else {
    			return false; 
    		}
    	}
    }
    
    public boolean queenMove(int originX, int originY, int destX,int destY) {
    	
    	Piece playerPiece = board[originX][originY]; 
    	
    	if(!playerPiece.getType().equals("Queen")) {
    		return false;
    	}
    	else {
    		//queen is moving diagonally
    		
    		if(playerPiece.diagonalMove() && verifyDiagonals(originX,originY,destX,destY)) {
    			return true; 
    		}
    		
    		else if(playerPiece.verticalMove() && verifyVerticals(originX,originY,destX,destY)) {
    			return true; 
    		}
    		
    		else if(playerPiece.horizontalMove() && verifyHorizontals(originX,originY,destX,destY)) {
    			return true; 
    		}
    		
    		else {
    			return false; 
    		}
    		
    	}
    }
    
    public boolean knightMove(int originX,int originY, int destX, int destY) {
    	
    	Piece playerPiece = board[originX][originY]; 
    	
    	if(!playerPiece.getType().equals("Knight")) {
    		return false; 
    	}
    	else {
    		
    		if(board[destX][destY] instanceof Piece) {
    			
    			if(board[destX][destY].isWhite != playerPiece.isWhite) {
        			capture(board[destX][destY]); 
        			board[destX][destY] = playerPiece; 
        			board[originX][originY] = null; 
        			return true; 
        		}
        		
        		else {
        			return false; 
        		}
    		}
    		
    		else {
    			board[destX][destY] = playerPiece; 
    			board[originX][originY] = null;
    			return true; 
    		}
    	}
    }
    
   public boolean checkForEnd(String input, int destX,int destY) {
	   String comp = Integer.toString(destX) + "," + Integer.toString(destY);
	   if(input.equals(comp)){
		   return true;
	   }
	   else {
		   return false; 
	   }
   }
   
   public boolean kingMove(int originX,int originY,int destX,int destY) {
   	
   	Piece playerPiece = board[originX][originY];
   	boolean endFlag = false; 
   	String currentMove = Integer.toString(destX) + "," + Integer.toString(destY); 
   	
   	if(!playerPiece.getType().equals("King")) {
   		return false; 
   	}
   	else {
   		
       	

   		if(board[destX][destY] instanceof Piece && board[destX][destY].isWhite != playerPiece.isWhite) {
   			capture(board[destX][destY]);
   			board[destX][destY] = playerPiece;
   			board[originX][originY] = null; 
   			endFlag = true; 
   		}
   		
   		else if(board[destX][destY] instanceof Piece && board[destX][destY].isWhite == playerPiece.isWhite) {
   			endFlag = false; 
   		}
   		
   		else if(!(board[destX][destY] instanceof Piece)) {
   			board[destX][destY] = playerPiece;
   			board[originX][originY] = null; 
   			endFlag = true; 
   		}
   	}
   	return endFlag; 
   }
    

    
    public boolean updateHelper(int originX, int originY, int destX, int destY) {
    	Piece playerPiece = board[originX][originY];
    	
    	// might need to rework this thing 
    	if(playerPiece.getType().equals("Pawn")) {
    		if(board[destX][destY] instanceof Piece) {
    			playerPiece.checkForKill(true);
    		}
    	}
    	
    	if(playerPiece.legalMove(originX, originY, destX, destY)) {
    		if(pawnMove(originX,originY,destX,destY)) {
    			return true; 
    		}
    		
    		else if(rookMove(originX,originY,destX,destY)) {
    			return true; 
    		}
    		
    		else if(bishopMove(originX,originY,destX,destY)) {
    			return true; 
    		}
    		else if(queenMove(originX,originY,destX,destY)) {
    			return true; 
    		}
    		else if(knightMove(originX,originY,destX,destY)) {
    			return true; 
    		}
    		else if(kingMove(originX,originY,destX,destY)) {
    			return true; 
    		}
    		else {
    			return false; 
    		}
    	}
    	
    	else {
    		return false; 
    	}
    }
    
    /* Gets the coordinates the user gave
     * Locates the piece and adds it if the move is legal
     * FIXED coordinates are correct   
     * @param origin
     * @param destination
     * @param turn
     */
    
    public void updateBoard(int [] origin, int [] destination,boolean turn) {
    	int fileO = origin[0], rankO = origin[1];
    	int fileD = destination[0], rankD = destination[1];
    	
    	
    	int originX = 8 - rankO, originY = fileO;
    	int destX = 8 - rankD, destY = fileD;
    	
    	Piece playerPiece = board[originX][originY]; 
    	    	
    	if(playerPiece instanceof Piece) {
    		if(updateHelper(originX,originY,destX,destY) && playerPiece.isWhite == turn) {
    			updateTextBoard(originX,originY,destX,destY); 
    			printUpdatedBoard(); 
    		}
    		else {
    			System.out.println("illegal move"); 
    		}
    		//System.out.println("Pieces captured so far: \n White: " + whiteCaptured + "\n Black: " + blackCaptured);
    	}
    }
    
    /**
     * Stores all pieces within the white and black row array
     */

    public void getRow(){
        for(int i = 0; i < 8; i++){

            if(i == 0 || i == 7){
                whiteRow[i] = whiteRook;
                blackRow[i] = blackRook;
            }

            if(i == 1 || i == 6){
                whiteRow[i] = whiteKnight;
                blackRow[i] = blackKnight;
            }

            if(i == 2 || i == 5){
                whiteRow[i] = whiteBishop;
                blackRow[i] = blackBishop;
            }

            if(i == 3){
                whiteRow[i] = whiteQueen;
                blackRow[i] = blackQueen;
            }

            if(i == 4){
                whiteRow[i] = whiteKing;
                blackRow[i] = blackKing;
            }
        }
    }


}