package game;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Game {
	
	/**
	 * The game object will control the overall 'flow' of the game
	 * We update the Board array after player input, then update the text board ->
	 * if the move is legal.
	 */

    public Board board;
    public boolean start; 
    public boolean isWhite; 
    public String winner; 
    int numLines = 0; 
    
    
    /**
     * The constructor for the game class
     * Whenever a new game object is created a game starts
     * 
     */
    public Game(boolean start,boolean isWhite){
    	board = new Board();
    	this.start = start; 
    	this.isWhite = isWhite; 
    }
    
    /**
     * Needs fixing, lots of edge cases 
     * @param userInput
     * @return
     */
    
    public boolean checkFormat(String userInput){
    	// has to be in the form '<char><rank> <char><rank>' 
    	char [] alp = {'a','b','c','d','e','f','g','h'};
    	int firstRank = userInput.charAt(1) - '0';  
    	int secondRank = userInput.charAt(4) - '0'; 
    	int checker = 0; 
    	    	    	
    	//check for first file 
    	for(int i = 0; i < alp.length; i++){
    		if(alp[i] == userInput.charAt(0)) {
    			checker++; 
    		}
    	}
    	
    	//check second file 
    	for(int i = 0; i < alp.length; i++){
    		if(alp[i] == userInput.charAt(3)) {
    			checker++; 
    		}
    	}
    	
    	
    	//check for ranks in correct range
    	if(firstRank > 0 && firstRank < 9 && secondRank > 0 && secondRank < 9) {
    		checker++; 
    	}
    	
    	if(checker == 3) {
    		return true; 
    	}
    	
    	else {
    		return false; 
    	}
    
    }
    
    
    /**
     * This method is responsible for reading the input txt file
     */
    
    public void numLines() {
    	try {
 
    		FileReader reader = new FileReader("/Users/Carlos/Desktop/Software Methodology/Chess58/src/game/moves.txt");
    		BufferedReader br = new BufferedReader(reader); 
    		int length = 0; 
    		String line;
    		    		
    		while((line = br.readLine()) != null) {
				length++; 
    		}
    		numLines = length; 
    		reader.close(); 
    	}
    	
    	catch (IOException alert){
    		alert.printStackTrace();
    	}
    }
    
    public void gameStart(String filePath) {
    	
    	//get number of lines
    	numLines(); 
		
    	try {
    		
    		FileReader reader = new FileReader(filePath);
    		BufferedReader br = new BufferedReader(reader); 
    		int fileLine = 1 ; // for debugging
    		String line;
    		    		
    		while((line = br.readLine()) != null) {
				String [] originDest = line.split(" "); 
				
				if(fileLine == numLines) {
    				winner = new String(line);
    				break; 
    			}
				
    			if(fileLine % 2 != 0) {
    				isWhite = true; 
    				
    				System.out.println("White's turn: " + line + "\n");
    				fileLine++; 
    				
    				if(line.equals("resign")) {
    					winner = "Black wins"; 
    					break; 
    				}
    				
        			//do stuff for game movement
    				if(checkFormat(line)) {
    					board.convert(originDest[0],originDest[1],isWhite); 
    				}
    			}
    			
    			else {
    				System.out.println("Black's turn: " + line + "\n");
    				isWhite = false; 
    				fileLine++; 
    				
    				if(line.equals("resign")) {
    					winner = "White wins"; 
    					break; 
    				}
        			    				
    				if(checkFormat(line)) {
    					board.convert(originDest[0],originDest[1],isWhite); 
    				}
    			}
    			
    		}
    		
    		System.out.println(winner);
    		reader.close(); 
    		
    	}
    	
    	catch (IOException alert){
    		alert.printStackTrace();
    	}
    }
    
		

    
    /**
     * Keep track of when a game of chess starts and when one ends
     * FALSE means it isn't over
     * TRUE means the game is over 
     * @param start
     * @return
     */
    
    public boolean isOver() {
    	return start; 
    }

}