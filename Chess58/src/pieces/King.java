package pieces;

import java.util.ArrayList;

public class King extends Piece{
	
	public ArrayList<String> possibleMoves;
	boolean isWhite;
	String type;
	
	public King(boolean isWhite,String type) {
		super(isWhite,type);
		this.isWhite = isWhite; 
		this.type = type; 
	}
	
	public ArrayList<String> allPossibleMoves(){
		return possibleMoves;
	}
	
	public boolean legalMove(int originX, int originY, int destX, int destY) {
		
		possibleMoves = new ArrayList<String>();
		allPossibleMoves = new ArrayList<String>(); 
		String userInput = Integer.toString(destX) + "," + Integer.toString(destY);
		String possibleMove; 
		int indexI,indexJ; 
		
		//all upward movement
		if(originX - 1 >= 0) {
			indexI = originX;
			indexI--; 
			
			//check right upward diagonal
			if(originY + 1 < 8) {
				indexJ = originY; 
				indexJ++; 
				
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
				possibleMoves.add(possibleMove); 
			}
			
			if(originY - 1 >= 0) {
				indexJ = originY; 
				indexJ--; 
				
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
				possibleMoves.add(possibleMove);
			}
			
			possibleMove = Integer.toString(indexI) +"," + Integer.toString(originY); 
			possibleMoves.add(possibleMove);
		}
		
		//all downward movement
		if(originX + 1 < 8) {
			indexI = originX;
			indexI++; 
			
			//check downward right diagonal
			if(originY + 1 < 8){
				indexJ = originY;
				indexJ++; 
				
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
				possibleMoves.add(possibleMove);
			}
			
			if(originY - 1 >= 0) {
				indexJ = originY; 
				indexJ--; 
				
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
				possibleMoves.add(possibleMove);
			}
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(originY); 
			possibleMoves.add(possibleMove);
		}
		
		//horizontal movement 
		
		if(originY + 1 < 8) {
			indexJ = originY; 
			indexJ++;
			
			possibleMove = Integer.toString(originX) + "," + Integer.toString(indexJ); 
			possibleMoves.add(possibleMove);
		}
		
		if(originY - 1 >= 0) {
			indexJ = originY; 
			indexJ--;
			
			possibleMove = Integer.toString(originX) + "," + Integer.toString(indexJ); 
			possibleMoves.add(possibleMove);
		}
		
		
		
		if(possibleMoves.contains(userInput)) {
			return true; 
		}
		
		else {
			return false; 
		}	
	}
}
