package pieces;

import java.util.ArrayList;
import java.util.Collections;

public class Rook extends Piece {
	public ArrayList<String> possibleMoves; 
	public boolean isWhite;
	public String type; 
	public ArrayList<Integer> horizontalIndices,verticalIndices;
	public ArrayList<String> horizontalMoves, verticalMoves,allPossibleMoves = new ArrayList<String>(); 
	boolean horizontalCheck,verticalCheck; 
	
	public Rook(boolean isWhite, String type) {
		super(isWhite,type); 
		this.isWhite = isWhite; 
		this.type = type; 
	}
	
	public boolean horizontalMove() {
		if(horizontalCheck) {
			return true; 
		}
		else {
			return false; 
		}
	}
	
	public boolean verticalMove() {
		if(verticalCheck) {
			return true;
		}
		else {
			return false; 
		}
	}
	
	public void addToList(String move) {
		allPossibleMoves.add(move); 
	}
	
	public ArrayList<String> allPossibleMoves() {
		return allPossibleMoves; 
	}
	
	/**
	 * Slightly different than the other pieces, uses a set to remove duplicate values
	 * A set is used since the origin position is duplicated
	 */
	
	public boolean legalMove(int originX, int originY, int destX, int destY) {
		horizontalIndices = new ArrayList<Integer>(); verticalIndices = new ArrayList<Integer>(); 
		horizontalMoves = new ArrayList<String>(); verticalMoves = new ArrayList<String>(); 
		String currentMove = Integer.toString(destX) + "," + Integer.toString(destY), possibleMove; 
		int indexI = originX,indexJ = originY; 
				
		//vertical movement 
		if(originX + 1 < 8) {
			while(indexI < 7) {
				indexI++; 
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(originY);
				verticalIndices.add(indexI); 
				verticalMoves.add(possibleMove);
				addToList(possibleMove); 
			}
		}
		
		if(originX - 1 > 0) {
			while(indexI > 0) {
				indexI--;
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(originY); 
				verticalIndices.add(indexI); 
				verticalMoves.add(possibleMove);
				addToList(possibleMove); 

			}
		}
		
		//horizontal movement
		
		if(originY + 1 < 8) {
			while(indexJ < 7) {
				indexJ++; 
				possibleMove = Integer.toString(originX) + "," + Integer.toString(indexJ); 
				horizontalIndices.add(indexJ); 
				horizontalMoves.add(possibleMove);
				addToList(possibleMove); 

			}
		}
		
		if(originY - 1 >= 0) {
			while(indexJ > 0) {
				indexJ--; 
				possibleMove = Integer.toString(originX) + "," + Integer.toString(indexJ);
				horizontalIndices.add(indexJ); 
				horizontalMoves.add(possibleMove);
				addToList(possibleMove); 
			}
		}
		
		Collections.sort(horizontalIndices);
		Collections.sort(verticalIndices); 

		if(horizontalMoves.contains(currentMove)) {
			horizontalCheck = true;
			return true; 
		}
		
		else if(verticalMoves.contains(currentMove)) {
			verticalCheck = true; 
			return true; 
		}
		
		else {
			return false; 
		}	
	}
	
	
	public ArrayList<Integer> horizontalIndices() {
		return horizontalIndices; 
	}
	
}
