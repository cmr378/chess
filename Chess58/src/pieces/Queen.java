package pieces;

import java.util.ArrayList;

public class Queen extends Piece{
	public ArrayList<String> possibleMoves,diagonalMoves,verticalMoves,horizontalMoves; 
	public boolean isWhite, verticalMove, horizontalMove,diagonalMove;
	public String type; 
	
	public Queen(boolean isWhite,String type) {
		// TODO Auto-generated constructor stub
		super(isWhite,type); 
		this.isWhite = isWhite; 
		this.type = type; 
	}
	
	public boolean diagonalMove() {
		return diagonalMove; 
	}
	
	public boolean horizontalMove() {
		return horizontalMove;
	}
	
	public boolean verticalMove() {
		return verticalMove; 
	}
	
	public boolean legalMove(int originX, int originY, int destX, int destY) {
		String possibleMove; 
		String currentMove = Integer.toString(destX) + "," + Integer.toString(destY); 
		horizontalMoves = new ArrayList<String>(); verticalMoves = new ArrayList<String>();
		diagonalMoves = new ArrayList<String>();possibleMoves = new ArrayList<String>();
		
		
		int indexI = originX,indexJ = originY; 
		
		
		//Vertical traversal
		if(originX - 1 >= 0) {
			while(indexI > 0) {
				indexI--; 
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(originY);
				verticalMoves.add(possibleMove);
				possibleMoves.add(possibleMove);
			}
		}
		
		indexI = originX; //reset
		
		if(originX + 1 < 8) {
			while(indexI < 7) {
				indexI++; 
				possibleMove = Integer.toString(indexI) + "," + Integer.toString(originY);
				verticalMoves.add(possibleMove); 
				possibleMoves.add(possibleMove);
			}
		}
		
		//horizontal traversal
		
		if(originY + 1 < 8) {
			while(indexJ < 7) {
				indexJ++; 
				possibleMove = Integer.toString(originX) + "," + Integer.toString(indexJ);
				horizontalMoves.add(possibleMove); 
				possibleMoves.add(possibleMove);
			}
		}
		
		indexJ = originY; //reset 
		
		if(originY - 1 >= 0) {
			while(indexJ > 0){
				indexJ--; 
				possibleMove = Integer.toString(originX) + "," + Integer.toString(indexJ);
				horizontalMoves.add(possibleMove); 
				possibleMoves.add(possibleMove);
			}
		}
		
		indexI=originX;
		indexJ=originY; 
		
		//check left-upward diagonal

		while(indexI > 0 && indexJ > 0) {
			indexI--;
			indexJ--; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			diagonalMoves.add(possibleMove);
			possibleMoves.add(possibleMove); 
		}
		
		//check right upward diagonal
		indexI = originX;
		indexJ = originY; 
		
		while(indexI > 0 && indexJ < 7) {
			indexI--;
			indexJ++; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			diagonalMoves.add(possibleMove);
			possibleMoves.add(possibleMove); 
		}
		
		//check left downward diagonal

		indexI = originX;
		indexJ = originY; 
		
		while(indexI < 7 && indexJ > 0) {
			indexI++;
			indexJ--; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			diagonalMoves.add(possibleMove);
			possibleMoves.add(possibleMove); 
		}
		
		//check right-downward diagonal:

		indexI = originX;
		indexJ = originY; 
		
		while(indexI < 7 && indexJ < 7) {
			indexI++;
			indexJ++; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			diagonalMoves.add(possibleMove);
			possibleMoves.add(possibleMove); 
		}
		
		if(verticalMoves.contains(currentMove)) {
			verticalMove = true; 
			return true; 
		}
		
		else if(horizontalMoves.contains(currentMove)) {
			horizontalMove = true; 
			return true; 
		}
		
		else if(diagonalMoves.contains(currentMove)) {
			diagonalMove = true; 
			return true; 
		}
		
		else {
			return false; 
		}
	}	
}
