package pieces;

import java.util.ArrayList;

public class Knight extends Piece {
	public ArrayList<String> possibleMoves; 
	public boolean isWhite; 
	public String type; 
	
	public Knight(boolean isWhite, String type) {
		super(isWhite,type);
		this.isWhite = isWhite; 
		this.type = type; 
	}
	
	public boolean legalMove(int originX, int originY, int destX, int destY) {
		possibleMoves = new ArrayList<String>();
		String possibleMove, userDestination = Integer.toString(destX) + "," + Integer.toString(destY); 
		int indexI = originX, indexJ = originY;
		
		//upward possibleMoves right
		
		if(indexI - 2 > 0 && indexJ + 1 <= 7) {
			indexI-=2;
			indexJ++;
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			possibleMoves.add(possibleMove);
		}
		
		 indexI = originX; 
		 indexJ = originY;

		
		//upward possibleMoves left
		if(indexI - 2 > 0 && indexJ - 1 >= 0) {
			indexI-=2;
			indexJ--;
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			possibleMoves.add(possibleMove);

		}
		
		//downward possibleMoves right
		
		indexI = originX; 
		indexJ = originY;
		
		if(indexI + 2 <= 7 && indexJ + 1 <= 7) {
			indexI+=2;
			indexJ++;
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			possibleMoves.add(possibleMove);

			
		}
		
		//downward possibleMoves left
		
		indexI = originX; 
		indexJ = originY;
		
		if(indexI + 2 <= 7 && indexJ - 1 >= 0) {
			indexI+=2;
			indexJ--; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			possibleMoves.add(possibleMove);

			
		}
		
		
		//leftward possibleMoves up
		
		indexI = originX; 
		indexJ = originY;
		
		if(indexI - 1 > 0 && indexJ - 2 >= 0) {
			
			indexI-=1;
			indexJ-=2; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			possibleMoves.add(possibleMove);
			
		}
		
		//leftward possibleMoves down
		
		indexI = originX; 
		indexJ = originY;
		
		if(indexI + 1 <= 7 && indexJ - 2 >= 0) {
			indexI+=1;
			indexJ-=2; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			possibleMoves.add(possibleMove);
			
		}
		
		//righward possibleMoves up
		
		indexI = originX; 
		indexJ = originY;
		
		if(indexI - 1 > 0 && indexJ + 2 <= 7) {
			indexI-=1;
			indexJ+=2; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			possibleMoves.add(possibleMove);

		}
		
		//rightward moves down
		
		indexI = originX; 
		indexJ = originY;
		
		if(indexI + 1 <= 7 && indexJ + 2 <= 7) {
			indexI+=1;
			indexJ+=2; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ);
			possibleMoves.add(possibleMove);

		}
		
		
		
		if(possibleMoves.contains(userDestination)) {
			return true;
		}
		
		else {
			return false; 
		}
	}
}
