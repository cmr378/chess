package pieces;

import java.util.ArrayList;

public abstract class Piece {
	public ArrayList<Integer> horizontalIndices,verticalIndices; 
	public ArrayList<String> enPassant,allPossibleMoves = new ArrayList<String>(); 
	public boolean isWhite,verifyMoves; 
	public String type;	
	
	/**
	 * When a piece is created, it requires a color and the name of the piece 
	 * @param isWhite
	 * @param type
	 */

	public Piece(boolean isWhite, String type) {
		this.isWhite = isWhite; 
		this.type = type; 
	}
	
	/**
	 * Get the type of piece it is 
	 * @return
	 */
	
	public String getType() {
		return type; 
	}
	
	/**
	 * Checks whether or not a user move is a possible move for the current piece 
	 * @param originX
	 * @param originY
	 * @param destX
	 * @param destY
	 * @return
	 */
	public boolean legalMove(int originX, int originY, int destX, int destY) {
		return true; 
	}
	
	
	/**
	 * Get the color of the piece
	 */
	
	public boolean isWhite() {
		return isWhite; 
	}
	
	/**
	 * Checks to see if its a capture move
	 * @return
	 */

	public boolean captureMove() {
		return true; 
	}
	
	/**
	 * Returns true if the pawn is in capture position
	 * else false 
	 * @param check
	 */
	public void checkForKill(boolean check) {
		
	}
	
	/**
	 * Specifically for the pawn object
	 * @return
	 */
	public boolean promotion() {
		return false; 
	}
	
	public boolean enPassant() {
		return true; 
	}
	
	/**
	 * Checks for en'passant 
	 * @return
	 */
	public String enPassantCoordinate(int x, int y) {
		return Integer.toString(x) + "." + Integer.toString(y); 
	}
	
	public boolean diagonalMove() {
		return true; 
	}
	
	public boolean verticalMove() {
		return true; 
	}
	
	public boolean horizontalMove() {
		return true; 
	}
	
	public ArrayList<Integer> horizontalIndices() {
		return horizontalIndices; 
	}
	
	public ArrayList<Integer> verticalIndices(){
		return verticalIndices; 
	}
	public ArrayList<String> allPossibleMoves(){
		return allPossibleMoves; 
	}

	
}
