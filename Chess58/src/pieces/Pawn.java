package pieces;

import java.util.ArrayList;

public class Pawn extends Piece{
	public ArrayList<String> movementMoves = new ArrayList<String>(),killMoves = new ArrayList<String>(), 
							 doubleMove = new ArrayList<String>(), allPossibleMoves = new ArrayList<String>();
	ArrayList<String> returnList; 
	public boolean killValue; //a flag for whether or not a pawn can capture 
	public boolean killMove; 
	public String type;
	public boolean isWhite, change;  
	int moveCounter = 0,enPassantCounter = 0; 
	int count = 0; 
	public int confirm = 0, possibleCheck = 0; 
	public int destX = 0 ,destY = 0; 
	public boolean validPromotion,verifyMoves; 

	public Pawn(boolean isWhite,String type) {
		super(isWhite,type);
		this.isWhite = isWhite;
		this.type = type; 
	}
	
	public void addToList(String move) {
		allPossibleMoves.add(move); 
	}
	
	public ArrayList<String> allPossibleMoves(){
		return allPossibleMoves; 
	}
	
	/**
	 * Checks all available moves the Rook piece has 
	 * For black, decrement i and leave j alone since a rook can only move vertically
	 * Add check for diagonal pieces that can be killed! (NOT DONE YET)
	 * Looks for all possible moves, isn't moving anything just checks the indexes - 
	 * around the piece.
	 * May need two lists for pawn, kill list and move list
	 */
	
	public boolean legalMove(int originX, int originY, int destX, int destY){
		allPossibleMoves.clear(); 
		movementMoves.clear();killMoves.clear();  
		String possibleMovement,possibleKill,currentMove = Integer.toString(destX) + "," + Integer.toString(destY);
		int indexI = originX,indexJ = originY; 
		confirm = 0; // resets capture flag
		
		//white movement is bottom top
		if(isWhite) {
						
			//first pawn move
			if(moveCounter == 0) {
				moveCounter++;
				indexI-=2;
				possibleMovement = Integer.toString(indexI) + "," + Integer.toString(originY);
				addToList(possibleMovement); 
				doubleMove.add(possibleMovement);
				
			}
		
			if(originX - 1 >= 0) {
				indexI = originX; //reset
				indexI--; 
				
				if(enPassantCounter == 1) {
					enPassantCounter = 0; 
				}
				
				
				//check upward left diagonal
				if(originY - 1 >= 0 && killValue) {
					indexJ = originY;//reset
					indexJ--;
					possibleKill = Integer.toString(indexI) + "," + Integer.toString(indexJ);
					addToList(possibleKill); 
					addToList(possibleKill); 
					killMoves.add(possibleKill); 
				}
				
				//check upward left diagonal
				if(originY + 1 < 8 && killValue) {
					indexJ = originY;//reset
					indexJ++;
					possibleKill = Integer.toString(indexI) + "," + Integer.toString(indexJ);
					addToList(possibleKill); 
					killMoves.add(possibleKill); 
				}
				indexJ = originY; 
				possibleMovement = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
				addToList(possibleMovement); 
				movementMoves.add(possibleMovement);
			}
		}
		
		//black movement top bottom
		else {
			
			if(moveCounter == 0) {
				indexI +=2; 
				moveCounter++;
				possibleMovement = Integer.toString(indexI) + "," + Integer.toString(originY);
				addToList(possibleMovement); 
				doubleMove.add(possibleMovement);
			}
			
			if(originX + 1 <= 7) {
				indexI = originX;//reset
				indexI++;
				
				//check downward left diagonal
				if(originY - 1 >= 0 && killValue) {
					indexJ = originY;//reset
					indexJ--;
					possibleKill = Integer.toString(indexI) + "," + Integer.toString(indexJ);
					addToList(possibleKill); 
					killMoves.add(possibleKill); 
				}
				
				//check downward right
				if( originY + 1 <=7 && killValue ) {
					indexJ = originY;//reset
					indexJ++;
					possibleKill = Integer.toString(indexI) + "," + Integer.toString(indexJ);
					addToList(possibleKill); 
					killMoves.add(possibleKill); 
				}
				
				indexJ = originY; 
				possibleMovement = Integer.toString(indexI) + "," + Integer.toString(originY);
				addToList(possibleMovement); 
				movementMoves.add(possibleMovement);
				
				
			}
		}
		
		if(doubleMove.contains(currentMove)) {
			enPassantCounter++; 
			return true;
		}
		
		
		else if(movementMoves.contains(currentMove)) {
			if(destX == 0 || destX == 7) {
				validPromotion = true; 
			}
			return true; 
		}
		
		else if(killMoves.contains(currentMove)) {
			confirm = 1;
			if(destX == 0 || destX == 7) {
				validPromotion = true; 
			}
			return captureMove();
		}
		else {
			return false; 
		}
	}
	
	
	public boolean enPassant() {
		if(enPassantCounter == 1) {
			return true;
		}
		else {
			return false; 
		}
	}
	
	public boolean captureMove() {
		if(confirm == 1) {
			return true; 
		}
		else {
			return false; 
		}
	}
	
	/**
	 * Pawn promotion, if pawn is in the correct row untouched we promote!
	 * @param x
	 * @return
	 */
	
	public boolean promotion() {
		return validPromotion; 
	}
	
	/**
	 * Returns the counter value
	 * @return
	 */
	
	public int getMoveCount() {
		return count; 
	}
	
	/**
	 * Returns true if the pawn is in capture position
	 * else false 
	 * @param check
	 */
	public void checkForKill(boolean check) {
		if(check == true) {
			killValue = true; 
		}
		else {
			killValue = false;
		}
	}
	
}
