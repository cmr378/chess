package pieces;

import java.util.ArrayList;


public class Bishop extends Piece{
	public ArrayList<String> possibleMoves; 
	public boolean isWhite;
	public String type; 
	
	public Bishop(boolean isWhite, String type) {
		super(isWhite,type); 
		this.isWhite = isWhite; 
		this.type = type; 
	}
	
	/**
	 * Bishops move in straight diagonals only, their reach can stretch throughout the board
	 * Bishops can only be blocked by pieces of its own color 
	 * Doesn't matter the color, they share the same movement 
	 */
	public boolean legalMove(int originX, int originY, int destX, int destY){
		possibleMoves = new ArrayList<String>();
		String possibleMove, userDestination = Integer.toString(destX) + "," + Integer.toString(destY); 
		int indexI = originX, indexJ = originY; 
		
		
		
		//check left-upward diagonal

		while(indexI > 0 && indexJ > 0) {
			indexI--;
			indexJ--; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			possibleMoves.add(possibleMove); 
		}
		
		//check right upward diagonal
		indexI = originX;
		indexJ = originY; 
		
		while(indexI > 0 && indexJ < 7) {
			indexI--;
			indexJ++; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			possibleMoves.add(possibleMove); 
		}
		
		//check left downward diagonal

		indexI = originX;
		indexJ = originY; 
		
		while(indexI < 7 && indexJ > 0) {
			indexI++;
			indexJ--; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			possibleMoves.add(possibleMove); 
		}
		
		//check right-downward diagonal:

		indexI = originX;
		indexJ = originY; 
		
		while(indexI < 7 && indexJ < 7) {
			indexI++;
			indexJ++; 
			
			possibleMove = Integer.toString(indexI) + "," + Integer.toString(indexJ); 
			possibleMoves.add(possibleMove); 
		}
		
		if(possibleMoves.contains(userDestination)) {
			return true;
		}
		
		else {
			return false; 
		}
	}
}
